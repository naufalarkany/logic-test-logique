function findMax(arr) {
  let max = arr[0];
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] > max) {
      max = arr[i];
    }
  }
  return max;
}

const number = [11, 6, 31, 201, 99, 861, 1, 7, 14, 79];
const maxNumber = findMax(number);
console.log("Max Number:", maxNumber);
