function isPrime(number) {
  // Numbers less than 2 are not prime
  if (number < 2) {
    return false;
  }

  // Check if there are factors from 2 to half of the number
  for (let i = 2; i <= number / 2; i++) {
    if (number % i === 0) {
      return false;
    }
  }

  // If there are no factors other than 1 and the number itself, it's prime
  return true;
}

// Test the function with some numbers
let number1 = 7;
let number2 = 12;

console.log(number1 + " is a prime number? " + isPrime(number1));
console.log(number2 + " is a prime number? " + isPrime(number2));
