function generateTriangle(rows) {
  let output = "";
  for (let i = 1; i <= rows; i++) {
    let rowString = "";
    for (let j = 1; j <= i; j++) {
      rowString += j + " ";
    }
    output += rowString.trim() + "\n";
  }
  return output;
}

const rows = 8;
const triangle = generateTriangle(rows);
console.log(triangle);
