function bubbleSort(arr) {
  let swapped;
  for (let i = 0; i < arr.length - 1; i++) {
    swapped = false;
    for (let j = 0; j < arr.length - i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        [arr[j], arr[j + 1]] = [arr[j + 1], arr[j]];
        swapped = true;
      }
    }
    if (!swapped) {
      break;
    }
  }
  return arr;
}

const number = [99, 2, 64, 8, 111, 33, 65, 11, 102, 50];
const sortedNumber = bubbleSort(number);
console.log("Sorted Numbers:", sortedNumber);
